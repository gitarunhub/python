# I want to print Hello world on my output console
# print can be called as a print function ie one or more python program lines grouped together for single cause it is predefined commands in python
# anything we represent in single or double quote ("or ') is treated as string data
print('Hellow world')

#I want to store a value 3 in a variable and print that on the console
a = 3
print(a)


#I want to store the data in a variable and print it in the output
name = "Arun"
print(name)

#we can print multiple value at a time
a = 6
b = 8.9
c = "Hello"
print (a,b,c)

# we can give values for multiple variables at a time
a,b,c = 5,11.2,"hi"
print (a)
print (b)
print (c)

#In python programing any number is called as integer eg ; 5
#Any decimal number is called float : 4.5
# now we will see how the integer is saved inside the ram
#for this we use type function

print (type(b))
# the output will be class "float" (floating)

print (type(a))
# output will be "int" (integer)

print(type(c))
# output will be "str" (string)